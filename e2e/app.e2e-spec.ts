import { PgsUsersPage } from './app.po';

describe('pgs-users App', () => {
  let page: PgsUsersPage;

  beforeEach(() => {
    page = new PgsUsersPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
