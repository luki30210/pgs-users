import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router'

import { HomeComponent } from '../home/home.component';
import { UsersListComponent } from '../users-list/users-list.component';
import { UserAddComponent } from '../user-add/user-add.component';
import { UserDetailsComponent } from '../user-details/user-details.component';


const routes: Routes = [
    //{ path: '', redirectTo: '/', pathMatch: 'full' },
    { path: '', component: HomeComponent, pathMatch: 'full' },
    { path: 'users', component: UsersListComponent, pathMatch: 'full' },
	{ path: 'users/add', component: UserAddComponent, pathMatch: 'full' },
    { path: 'users/show/:login', component: UserDetailsComponent, pathMatch: 'full' }
];

@NgModule({
	imports: [
		RouterModule.forRoot(routes)
	],
	exports: [
		RouterModule
	]
})
export class AppRoutingModule { }

export const routingComponents = [
    HomeComponent,
    UsersListComponent,
	UserDetailsComponent,
	UserAddComponent
];
