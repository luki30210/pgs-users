import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { User } from '../model/user.model';

@Injectable()
export class UsersService {

	//private _url: string = "http://localhost:8080";
	private _url: string = "http://" + window.location.hostname + ":8080";	// DO TESTÓW!

	constructor(private _http: Http) { }

	getUsers() {
		return this._http.get(this._url + '/users')
			.map((response: Response) => response.json())
			.catch(this._errorHandler);
	}

	getUser(login: string) {
		return this._http.get(this._url + '/users/' + login)
			.map((response: Response) => response.json())
			.catch(this._errorHandler);
	}

	_errorHandler(error: Response) {
		console.error(error);
		return Observable.throw(error || "Server error");
	}

	addUser(user: User) {
		let body = JSON.stringify(user);
		let headers = new Headers({ 'Content-Type': 'application/json' });
		let options = new RequestOptions({ headers: headers });
		return this._http.post(this._url + '/users', body, options);
	}

}
