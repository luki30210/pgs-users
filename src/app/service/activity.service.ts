import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { Activity } from '../model/activity.model';
//import { ActivityFactory } from '../model/activity.factory';

@Injectable()
export class ActivityService {

	//private _url: string = "http://localhost:8080";
    private _url: string = "http://" + window.location.hostname + ":8080";	// DO TESTÓW!

    constructor(private _http: Http) { }

    getActivities() {
  	  return this._http.get(this._url + '/activities')
  		  .map((response: Response) => response.json())
  		  .catch(this._errorHandler);
    }

	getUserActivities(login: string) {
  	  return this._http.get(this._url + '/users/' + login + '/activities')
  		  .map((response: Response) => response.json())
  		  .catch(this._errorHandler);
    }

    getActivity(id: number) {
  	  return this._http.get(this._url + '/activities/' + id)
  		  .map((response: Response) => response.json())
  		  .catch(this._errorHandler);
    }

    _errorHandler(error: Response) {
  	  console.error(error);
  	  return Observable.throw(error || "Server error");
    }

    addActivity(activity: Activity) {
  	  let body = JSON.stringify(activity);
  	  let headers = new Headers({ 'Content-Type': 'application/json' });
  	  let options = new RequestOptions({ headers: headers });
  	  return this._http.post(this._url + '/activities', body, options);
    }

}
