import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { ReactiveFormsModule } from '@angular/forms'    // Model Driven Forms

import { AppComponent } from './app.component';
import { AppRoutingModule, routingComponents } from './app-routing/app-routing.module';

@NgModule({
	declarations: [
		AppComponent,
		routingComponents
	],
	imports: [
		BrowserModule,
		FormsModule,
		HttpModule,
		//RouterModule,
		AppRoutingModule,
		ReactiveFormsModule
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
