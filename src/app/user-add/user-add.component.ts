import { Component, OnInit } from '@angular/core';
import { FormGroup, /*FormControl*/ FormBuilder, Validators } from '@angular/forms';


import { User } from '../model/user.model';
import { UserFactory } from '../model/user.factory';
import { UsersService } from '../service/users.service';

@Component({
	selector: 'app-user-add',
	templateUrl: './user-add.component.html',
	styleUrls: ['./user-add.component.css'],
	providers: [UserFactory, UsersService]
})
export class UserAddComponent implements OnInit {

	userForm: FormGroup;
	public userAdded: boolean;
	public statusCode: number;

    constructor(private _formBuilder: FormBuilder, private _usersService: UsersService, private _userFactory: UserFactory) { }

    ngOnInit() {
		this.userAdded = false;
		this.statusCode = 0;

        this.userForm = this._formBuilder.group({
			login: [null, [Validators.required, Validators.minLength(4)]],
            firstName: [null, [Validators.required]],
			lastName: [null, [Validators.required]],
            dateOfBirth: [null, [Validators.required]],
			email: [null, [Validators.required]],
			userType: [1, [Validators.required]]
        })
    }

    onSubmit() {
		let formData = this.userForm.value;

		let user = this._userFactory.create({
			"login": formData.login,
			"firstName": formData.firstName,
			"lastName": formData.lastName,
			"dateOfBirth": formData.dateOfBirth,
			"email": formData.email,
			"userType": formData.userType
		});

		this._usersService.addUser(user).subscribe(
			data => {
				this.userAdded = true;
				this.statusCode = data.status;
			},
			error => {
				this.userAdded = false;
				this.statusCode = error.status;
			});

	}

	//TESTOWANIE
	onControlShiftA() {
		this.userForm.patchValue({
			login: 'luki30210',
            firstName: 'Łukasz',
			lastName: 'Patro',
            dateOfBirth: '1995-02-17',
			email: 'lukasz30210@gmail.com',
			userType: 1
		});
	}

}
