import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UsersService } from '../service/users.service';
import { User } from '../model/user.model';

@Component({
	templateUrl: './users-list.component.html',
	styleUrls: ['./users-list.component.css'],
	providers: [UsersService]
})
export class UsersListComponent implements OnInit {

    users: User[] = [];
    errorMsg: string;
    public selectedId: number;

	constructor(private _usersService: UsersService, private router: Router, private aRoute: ActivatedRoute) {}

	ngOnInit() {
		this._usersService.getUsers().
			subscribe(resUserData => this.users = resUserData,
			resUserError => this.errorMsg = resUserError);
	}

	onSelect(login: string) {
        this.router.navigate(['show/'+login], {relativeTo: this.aRoute})
    }

}
