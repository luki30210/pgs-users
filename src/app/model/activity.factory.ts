import { Activity } from './activity.model';

export class ActivityFactory {

	createEmpty() {
		return new Activity({ "id": 0, "name": "", "description": "", "startDate": "" });
	}

    create(activityInfo: Activity) {
		return new Activity(activityInfo);
	}

}
