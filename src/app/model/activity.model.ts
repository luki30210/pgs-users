export class Activity {

	id: number;
    name: string;
    description: string;
    startDate: string;

    constructor(activityInfo: { "id": number, "name": string, "description": string, "startDate": string }) {
        this.id = activityInfo.id;
		this.name = activityInfo.name;
		this.description = activityInfo.description;
		this.startDate = activityInfo.startDate;
    }

}
