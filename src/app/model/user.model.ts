export class User {

    login: string;
    firstName: string;
    lastName: string;
    dateOfBirth: string;
    email: string;
    userType: string;

    constructor(userInfo: { "login": string, "firstName": string, "lastName": string, "dateOfBirth": string, "email": string, "userType": string }) {
        this.login = userInfo.login;
        this.firstName = userInfo.firstName;
        this.lastName = userInfo.lastName;
        this.dateOfBirth = userInfo.dateOfBirth;
        this.email = userInfo.email;
        this.userType = userInfo.userType;
    }

}
