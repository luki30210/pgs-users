import {User} from './user.model';

export class UserFactory {

	createEmpty() {
		return new User({ "login": "", "firstName": "", "lastName": "", "dateOfBirth": "", "email": "", "userType": "" });
	}

    create(userInfo: User) {
		return new User(userInfo);
	}

}
