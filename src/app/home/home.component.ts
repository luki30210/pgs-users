import { Component, OnInit } from '@angular/core';

import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Component({
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

	constructor(private _http: Http) { }

	getSample() {
		return this._http.get("https://baconipsum.com/api/?type=all-meat&paras=20&start-with-lorem=1")
			.map((response: Response) => response.json())
			.catch(this._errorHandler);
	}

	_errorHandler(error: Response) {
		console.error(error);
		return Observable.throw(error || "Server error");
	}

	samples: any = [];
	errorMsg: string;

	ngOnInit() {
		this.getSample().subscribe(
			resSampleData => this.samples = resSampleData,
			resSampleError => this.errorMsg = resSampleError
		);
	}

}
