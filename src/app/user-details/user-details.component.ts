import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UsersService } from '../service/users.service';
import { UserFactory } from '../model/user.factory';
import { User } from '../model/user.model';
import { ActivityService } from '../service/activity.service';
import { ActivityFactory } from '../model/activity.factory';
import { Activity } from '../model/activity.model';

@Component({
	selector: 'app-user-details',
	templateUrl: './user-details.component.html',
	styleUrls: ['./user-details.component.css'],
	providers: [UsersService, UserFactory, ActivityService, ActivityFactory]
})
export class UserDetailsComponent implements OnInit {

	constructor(private _usersService: UsersService, private _userFactory: UserFactory, private _activityService: ActivityService, private _aRoute: ActivatedRoute) { }

	private user: User = this._userFactory.createEmpty();
    errorMsg: string;
	activities: Activity[] = [];

    ngOnInit() {

		this._aRoute.params.subscribe((params: Params) => { this.user.login = params['login'] });	//parseInt()

		this._usersService.getUser(this.user.login).
			subscribe(resUserData => this.user = resUserData,
			resUserError => this.errorMsg = resUserError);

		this._activityService.getUserActivities(this.user.login).
			subscribe(resUserActivitiesData => this.activities = resUserActivitiesData,
			resUserActivitiesError => this.errorMsg = resUserActivitiesError);
    }

}
